# DVD cloning by the CLI

This tooling is made so one can convert their DVD's to VOB and later on to MKV
with srt subs.

# Installing

You will need to have installed the following applications:

* zsh
* awk
* sed
* coreutils

## Ripping DVD

* mplayer
* ffmpeg
* ffprobe

# Subtitles

* mkvtoolnix
* uchardet
* vobsub2srt from https://github.com/waterkip/VobSub2SRT

This version contains patches that make it workable on Debian 12
vobsub2srt is dependent on tesseract:

* libtesseract-dev
* libtesseract5
* tesseract-ocr
* tesseract-ocr-dan
* tesseract-ocr-eng
* tesseract-ocr-fin
* tesseract-ocr-fra
* tesseract-ocr-isl
* tesseract-ocr-nld
* tesseract-ocr-nor
* tesseract-ocr-osd
* tesseract-ocr-por
* tesseract-ocr-script-latn
* tesseract-ocr-spa
* tesseract-ocr-swe

# LICENSES

This project ships with files as: BSD, GPLv2 or later and GPLv3 or later

* BSD: Some files that can be used everywhere and I use in my other projects
* GPLv2: ffmpeg, uchardet
* GPLv3: vob2srt
* Apache: tesseract

Since all licenses are GPL compatible you can relicense a fork to GPL-v3 or
later at your discretion.
