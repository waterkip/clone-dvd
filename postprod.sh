#!/usr/bin/env zsh

# SPDX-FileCopyrightText: 2021-2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: GPL-2.0-or-later

typeset -A subtitles;

if [ -z "$1" ]
then
  INFO=$(ls *.info)
else
  INFO=$1
fi

if [ ! -f $INFO ]
then
  echo "No INFO file" >&2
  exit 1
fi

basename=$(basename $INFO .info)

get_info_from_dvdinfo() {
    grep $1 $INFO | awk -F\= '{print $NF}'
}

nav=$(grep LENGTH $INFO | grep ID_DVD_TITLE | sort -t= -k2 -nr | head -1 | awk -F_ '{print $4}');
id=$(get_info_from_dvdinfo ID_DVD_DISC_ID)
name=$(get_info_from_dvdinfo ID_DVD_VOLUME_ID)
chapters=$(get_info_from_dvdinfo ID_CHAPTERS)

vob=$basename.vob
mkv=$basename.mkv
info=$basename.info

#https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes
typeset -A audio
audio[da]=dan
audio[de]=deu
audio[en]=eng
audio[es]=spa
audio[fi]=fin
audio[fr]=fra
audio[it]=ita
audio[nl]=dut
audio[no]=nor
audio[sv]=swe
audio[pt]=por
audio[is]=isl
audio[ar]=ara

init_audio_mapping() {

  local md_audio=""
  id=0;
  typeset -a languages
  languages=($(grep LANG $INFO | grep AID | sort -n));

  for i in $languages
  do
    lang=$(echo "$i" | awk -F\= '{print $NF}')
    map=${audio[$lang]}

    if [ -n "$map" ]
    then
        md_audio="$md_audio -metadata:s:a:$id language=$map"
    fi
    id=$(( id + 1 ))
  done
  echo $md_audio
}


get_subtitles() {
  typeset -a languages
  languages=($(grep LANG $INFO | grep ID_SID | sort -n));

  if [ ${#languages[@]} -eq 0 ]
  then
    languages=("ID_SID_0_LANG=nl")
  fi

  for i in $languages
  do

    id=$(echo "$i" | awk -F\_ '{print $3}')
    lang=$(echo "$i" | awk -F\= '{print $NF}')
    map=${audio[$lang]}
    [ -n "$map" ] && subtitles+=($id $map);
  done

}

init_subtitle_mapping() {
  local md_subs=""
  [ -z "$subtitles" ] && get_subtitles

  for key in "${(@k0n)subtitles}"; do
      md_subs="$md_subs -metadata:s:s:$key language=$subtitles[$key]"
  done
  echo $md_subs
}

get_mapping() {

    if [ ! -f $basename.mapping ]
    then
      grep -q ID_SUBTITLE_ID $INFO
      if [ $? -eq 0 ]
      then
       echo "-map 0:v -map 0:a -map 0:s"
      else
       echo "-map 0:v -map 0:a"
      fi
      return
    fi
    local map=$(grep -v '^#' $basename.mapping | sed -e 's/\#.*//' | tr '\n' ' ')
    echo "$map"
}

get_subtitles
metadata_audio=$(eval init_audio_mapping)
metadata_subs=$(eval init_subtitle_mapping)
mapping=$(eval get_mapping)

# https://stackoverflow.com/questions/19200790/converting-dvd-image-with-subtitles-to-mkv-using-avconv
eval ffmpeg \
    -hide_banner \
    -fflags +genpts \
    -analyzeduration 1000000k \
    -probesize 1000000k \
    -i $vob \
    -c copy \
    $mapping \
    $metadata_audio \
    $metadata_subs \
    -y \
    $mkv
