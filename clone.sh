#!/usr/bin/env zsh
set -x

# SPDX-FileCopyrightText: 2021-2024 Wesley Schwengle <wesleys@opperschaap.net>
#
# SPDX-License-Identifier: GPL-2.0-or-later

typeset -A subtitles;

dvd="/dev/sr0"

response="$(mplayer \
        -ao null \
        -vo null \
        -nosound \
        -nocache \
        -really-quiet \
        -identify \
        -frames 0 \
        -dvd-device $dvd dvd://
        )"

get_info_from_dvdinfo() {
    echo "$response" | grep $1| awk -F\= '{print $NF}'
}

dir="$name/$id"
basename="$dir/$name"

vob=$basename.vob
info=$basename.info

if [ ! -d $dir ]
then
    mkdir -p $dir
    echo "$response" > $info
fi

mplayer -dvd-device $dvd dvd://$nav -dumpstream -dumpfile $vob

